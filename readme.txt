APB server readme

Before you start, this server repo does not contain the game data files please copy the always.dat (and all other alwaysxx.dat files) and the mix files from the latest apb version 
from the launcher to the data folder of the server (don't overwrite anything that is already there).


For the first time use configure server.ini (it contains comments what does what) with the ports you want to use
To make the server apear on the launcher make sure you block the GameSpyQueryPort set in server.ini
(this can be done with a firewall even the windows firewall or your router).
configure SSGM.ini (most basic settings are set but make sure the port set there is blocked too as else people can 
use it to remote access the apb server.
configure data/APB_Public.ini this contains settings like the playerlimit starting credits etc. (everything is set to the same values as the official server)

the map rotation is set in tt.cfg unless mapguard is being used then use brenbot/plugins/MapGuard.xml and make sure the rotation in tt.cfg consists of only 1 map
------Brenbot

configure the Brenbot's main config file: Brenbot/brenbot.cfg
brenbot.cfg is the config file where the basic bot settings are set like the IRC admin channel public channel (admin channel IS REQUIRED)
most options accept the channel and the FDSLogfilepath and FDSconfigfilepath are set to be ready to go if the path of the server is c:/server/apb else you can change it to the location where the server is located and the logfile path is.
make sure you register your botname on irc before use with nickserv and use those login details in the irc auth settings area of brenbot.cfg

Moderators are defined in brenbot/moderators.cfg and by default need to have their nick registered with brenbot by messaging the bot on irc in
a private message using !register nick password
once they join they need to authenticate either by using "!auth nick" in the admin channel if they have +h (halfop) or by messaging brenbot on irc
"!auth nick password" when using !auth nick" and the autoauth plugin is loaded the person will be autoauthed based on IP the next time they join
the requirement of having a registered nick can be disabled in brenbot.cfg the drawback is that everyone with a name defined in moderators.cfg
will be automatically authenticated.

to make the server appear on the launcher you will need to set up the port in "brenbot/plugins/gamespy.xml" that you want to use 
as the gamespyquery port this MUST be different then the one in server.ini and needs to be allowed by the firewall.

all plugins are unloaded by default so once the server is running make sure you have someone with founder or super operator (+a or +q)
in the admin channel and load atleast the gamespy plugin using "!plugin_load gamespy"
some other recommended plugins are mute, autoauth and MapGuard. for a full list use !plugins to see what is loaded (green) and what isn't (red)



for more help see the comments in the files.
if more help is needed you can highlight triattack on discord or post on the forums
