#
# Spectate plugin for BRenBot 1.51 by Daniel Paul
#
# Version 1.00
#
package spectate;

use POE;
use plugin;

# define additional events in the POE session

our %additional_events =
(
	# !command functions
	"spectate" => "spectate",
	"newspectate" => "newspectate"
);

# BRenBot automatically sets the plugin name
our $plugin_name;

# BRenBot automatically imports the plugin's config (from the xml file) into %config
our %config;

our $currentVersion = 1.00;


########### Functions for !commands

# Set spectate mode on a player
sub spectate
{
	my %args = %{@_[ ARG0 ]};

	# Check they supplied a name or ID to search for, if they have not bail out
	if ( !$args{arg1} )
	{
		my $syntaxvalue = $args{settings}->{syntax}->{value};
		$message = "Usage: $syntaxvalue";
		return;
	}

	# Try to find the player by name or ID
	my ( $result, %player ) = plugin::getPlayerData( $args{'arg1'} );
	if ( $result == 1 )
	{
		# Player found, spectate them
		plugin::RenRemCMD("spectate $player{id}");
		plugin::ircmsg ("10[Spectate] Player $player{name} is now in spectate mode.",'A');
	}
	else
	{
		if ( $args{nicktype} == 1 ) { plugin::ircmsg ( "Player $args{arg1} not found ingame", $args{'ircChannelCode'} ); }
		else { plugin::pagePlayer ( $args{'nick'}, "Player $args{arg1} not found ingame" ); }
	}
}
# Set spectate mode on a player
sub newspectate
{
	my %args = %{@_[ ARG0 ]};

	# Check they supplied a name or ID to search for, if they have not bail out
	if ( !$args{arg1} )
	{
		my $syntaxvalue = $args{settings}->{syntax}->{value};
		$message = "Usage: $syntaxvalue";
		return;
	}

	# Try to find the player by name or ID
	my ( $result, %player ) = plugin::getPlayerData( $args{'arg1'} );
	if ( $result == 1 )
	{
		# Player found, spectate them
		plugin::RenRemCMD("toggle_spectator  $player{id}");
		plugin::ircmsg ("10[Spectate] Player $player{name} is now in the new spectate mode.",'A');
	}
	else
	{
		if ( $args{nicktype} == 1 ) { plugin::ircmsg ( "Player $args{arg1} not found ingame", $args{'ircChannelCode'} ); }
		else { plugin::pagePlayer ( $args{'nick'}, "Player $args{arg1} not found ingame" ); }
	}
}


########### Event handlers

sub start
{
	plugin::set_global ( "version_plugin_spectate", $currentVersion );
}


sub stop
{
	# Nothing to do

}


sub command
{
	my $kernel = $_[KERNEL];
	my %args = %{@_[ ARG0 ]};
	$kernel->yield( $args{'command'} => \%args);
}


# Return true or the bot will not work properly...
1;