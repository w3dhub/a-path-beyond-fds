#
# Tagging System for BRenBot 1.53 Build 13 by Catalyst
#
# Version 1.0
#
package tags;

use POE;
use plugin;
use modules;
use POSIX;
use brdatabase;

our %additional_events = 
(
	"tag" => "tag"
);

our $plugin_name;

our %config;

our %autotagged;
our $autotaggedlist;
our $dbc;
our $dbVersion;
our $currentVersion = '1.0';
our %autotag;


my $loaded_ok = undef;

# Check bot compatibility
if ( !defined(plugin::getBrVersion) || plugin::getBrVersion() < 1.53
  || (plugin::getBrVersion() == 1.53 && plugin::getBrBuild() < 15 ) )
{
  print ' == Donate Plugin Error ==\n'
      . 'This version of the plugin is not compatible with BRenBot versions older than 1.53.15\n';
  return 0;
}



sub tag
{
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	my %args = %{$args};
	if ( $args{'arg'} =~ m/^\!\S+\s(\S*)\s*(.*)/i )
	{
		my $tagged = $1;
		my $tag = $2;
		my $tagz = "tag";
		eval
		{
			# Check they have entered a person to tag and the tag for taging them
		   if ( !$tag || !$tagged )
		   {
				my $syntaxvalue = $args{settings}->{syntax}->{value};

				if ( $args{'nicktype'} == 1 ) { plugin::ircmsg( "Usage: $syntaxvalue", $args{'ircChannelCode'} ); }
				else { modules::pagePlayer ( $args{'nick'}, "BRenBot", "Usage: $syntaxvalue" ); }
				return;
			}

			#Get player details (Works for both ID and name). Return if they are not found.
			my ( $result, %player ) = playerData::getPlayerData ( $tagged );
			if ( $result != 1 )
			{
				print("$result\n");
				if ( $args{'nicktype'} == 1 ) { plugin::ircmsg ( "[TAG] ERROR: $tagged was not found ingame, or is not unique.", $args{'ircChannelCode'} ); }
				else { plugin::RenRemCMD ( "msg [BR] TAG ERROR: $tagged was not found ingame, or is not unique." ); }
				return;
			}
			plugin::RenRemCMD ("$tagz $player{'id'} $tag");
			savetodatabase( $player{'name'}, $args{'nick'}, $tag );
		}	# End of eval
		or modules::display_error($@);
	}
} # End of Tagging

sub savetodatabase
{
	my $name = lc(shift);			$name =~ s/'/''/g;
	my $tagger = shift;			$tagger =~ s/'/''/g;
	my $tag = shift;			$tag =~ s/'/''/g;

	$dbc->do( "DELETE FROM tags WHERE LOWER(name) = '$name'",);
	$dbc->do( "INSERT INTO tags ( name, tagger, tag ) VALUES ( '$name', '$tagger', '$tag' )",);

	# Write the tags to the logs table
	brdatabase::writeLog ( 11, "[TAG] $name was Tagged by $tagger - $tag", time() );
}

########### Event handlers

sub start
{
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	my %args = %{$args};

		# Set our current version in the globals table
	plugin::set_global ( "version_plugin_tags", $currentVersion );

	# Now connect to, and if necessary create, the database
	my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$dbTime,$ctime,$blksize,$blocks) = stat('tags.dat');
	
	$dbc = DBI->connect("dbi:SQLite:dbname=tags.dat","","");
		
	if( !$dbTime )
	{
		print "Tagging PLUGIN :: Database does not exist, creating fresh database\n";
		
		$dbc->do(
			"CREATE TABLE tags 
			(name 			char(128)		PRIMARY KEY
			,tagger 		int
			,tag 			int)"
		);

		$dbc->do( "CREATE TABLE Tagging_plugin ( version int PRIMARY KEY)" );
 		
		$dbc->do( "INSERT INTO Tagging_plugin(version) VALUES ($currentVersion)" );
	}
  return ($loaded_ok = 1);
}

sub stop
{
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	
	# Comamnds to run when BRenBot quits
	$dbc->disconnect;
}

sub playerjoin
{
	# triggers on players joining
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	my %args = %{$args};
	#plugin::ircmsg("Hey look ".lc($args{nick})." Joined the game");
	my $name = lc($args{nick});		$name =~ s/'/''/g;
	my @row = $dbc->selectrow_array ( "SELECT name, tag FROM tags WHERE LOWER(name) = \"".lc($args{nick})."\"" );
	my ( $tagee, $tag ) = @row;
	#plugin::ircmsg("So my Name is ".lc($args{nick})." and the Guy who tagged me was ".$tagee." and the tag is ".$tag." ... What a Bastard");
	#plugin::ircmsg(lc($args{name})."'s ID is: ".$args{id});
	RenRem::RenRemCMD("tag ".$args{id}." ".$tag);
	#plugin::ircmsg("tag ".$args{id}." ".$tag);
}

sub command
{
	# trigger on !commands
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	my %args = %{$args};
	
	# each command from IRC/ingame comes here first. have to trigger the approriate functions from here
	if ( $args{command} eq "tag" )
	{
		$kernel->yield("tag" => \%args);
	}
}

sub text
{
	# trigger on any text ingame or irc
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	my %args = %{$args};
}
1;