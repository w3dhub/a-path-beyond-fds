package MolotovBrothers;

use POE;
use plugin;
use modules;
use playerData;

my $loaded_ok = undef;

if ( !defined(plugin::getBrVersion) || plugin::getBrVersion() < 1.53 || (plugin::getBrVersion() == 1.53 && plugin::getBrBuild() < 18 ) )
{
  print ' == MolotovBrothers Plugin Error ==\n' . 'This version of the plugin is not compatible with BRenBot versions older than 1.53.18\n';
  return 0;
}

# BRenBot supplies these on plugin startup
our $plugin_name;
our %config;

our %additional_events =
(
  "molotov" => "molotov",
  "lastmolotov" => "lastmolotov",
  "GL_InfantryKill" => "GL_InfantryKill"
);

my $isDanielAlive = 0;
my $isYuriAlive = 0;

# Get the presets name from the config file
my $DanielPreset = "Molotov_Dan";
my $YuriPreset = "Molotov_Yuri";

# Define additional events in the POE session
sub start
{
  return ($loaded_ok = 1);
}

sub stop
{
	
}

sub command
{
	return if ($loaded_ok == 0);

 	my %args = %{$_[ARG0]};
 	$_[KERNEL]->yield ( $args{'command'} => \%args );
}

sub molotov
{
	my %args = %{@_[ ARG0 ]};
	my $calledFromIRC = $args{'nicktype'};
	my $callerName = $args{'nick'};

	if ( !$args{'arg1'} || !$args{'arg2'} )
	{
		SendMessageToGameOrIRC($calledFromIRC, $callerName, "Not enough arguments. The syntax is !molotov <DanielPlayer> <YuriPlayer>");
		return;
	}

	# Selected players
	my $DanielPlayerName = "$args{'arg1'}";
	my $YuriPlayerName = "$args{'arg2'}";

	# Get their IDs
	my ($findResultD, $DanielPlayerID) = playerData::playerNameToID($DanielPlayerName);
	my ($findResultY, $YuriPlayerID) = playerData::playerNameToID($YuriPlayerName);

	if ($findResultD == 0 || $findResultY == 0)
	{
		SendMessageToGameOrIRC($calledFromIRC, $callerName, "One or both players not found!");
		return;		
	}
	elsif($findResultY == 2 || $findResultD == 2)
	{
		SendMessageToGameOrIRC($calledFromIRC, $callerName, "One or both player's name not unique!");
		return;
	}

	plugin::ircmsg("7DEBUG: Player $DanielPlayerName ID $DanielPlayerID  Preset $DanielPreset", "A");
	plugin::ircmsg("7DEBUG: Player $YuriPlayerName ID $YuriPlayerID  Preset $YuriPreset", "A");

	plugin::RenRemCMD("team $DanielPlayerID 2");
	plugin::RenRemCMD("ChangeChar $DanielPlayerID $DanielPreset");
	plugin::RenRemCMD("team $YuriPlayerID 2");
	plugin::RenRemCMD("ChangeChar $YuriPlayerID $YuriPreset");
	plugin::RenRemCMD("givepoints $DanielPlayerID 1000");	
	plugin::RenRemCMD("givepoints $YuriPlayerID 1000");

	$isDanielAlive = 1;
	$isYuriAlive = 1;		

	# Page teams 0 - Sovs, 1 - Allies
	plugin::RenRemCMD("cmsgt 0 255,90,120 $config{'SovsMsgP1'}");
	plugin::RenRemCMD("cmsgt 0 255,90,120 $config{'SovsMsgP2'}");
	plugin::RenRemCMD("cmsgt 1 255,90,120 $config{'AllMsgP1'}");
	plugin::RenRemCMD("cmsgt 1 255,90,120 $config{'AllMsgP2'}");

	# Page Daniel and Yuri
	plugin::pagePlayer($DanielPlayerName, "W3D", $config{'MolotovMsgP1'} );
	plugin::pagePlayer($YuriPlayerName, "W3D", $config{'MolotovMsgP1'} );

	plugin::pagePlayer($DanielPlayerName, "W3D", $config{'MolotovMsgP2'});
	plugin::pagePlayer($YuriPlayerName, "W3D", $config{'MolotovMsgP2'});

	plugin::pagePlayer($DanielPlayerName, "W3D", $config{'MolotovMsgP3'});
	plugin::pagePlayer($YuriPlayerName, "W3D", $config{'MolotovMsgP3'});

	# Display in irc
	plugin::ircmsg("7 Player $DanielPlayerName is now playing as Daniel", "A");
	plugin::ircmsg("7 Player $YuriPlayerName is now playing as Yuri", "A");

	plugin::execute_query("INSERT INTO plugin_molotov(danielPlayer, yuriPlayer, timeSummoned) VALUES ( '$DanielPlayerName', '$YuriPlayerName'," .time().")", 1);	
}

sub lastmolotov
{

	my @molotovData = plugin::execute_query("SELECT * FROM plugin_molotov WHERE ID = (SELECT MAX(ID) FROM plugin_molotov)");

	if (scalar(@molotovData) > 0)
	{
		my $timeStampConverted = plugin::strftimestamp('%d/%m/%Y at %H:%M:%S', $molotovData[0]->{'timesummoned'});
		plugin::ircmsg("7$molotovData[0]{'danielplayer'} and $molotovData[0]->{'yuriplayer'} played as Molotov Brothers at $timeStampConverted ", "A");
	}
	else
	{
		plugin::ircmsg("7 Nothing to print", "A");	
	}
}

sub SendMessageToGameOrIRC
{
	my ($calledFromIRC, $callerName, $message) = @_;

	if ($calledFromIRC == 1) 
		{ 
			plugin::ircmsg("7$message", "A");
		}
		else
		{
			plugin::pagePlayer($callerName, "W3D", "$message");
		}
}

sub GL_InfantryKill
{
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	my $line = $args->{line};
	
	#KILLED;SOLDIER;1500005425;Allied_Rifle_Soldier;-297;46;-10;-86;1500004711;Soviet_Rifle_Soldier;-352;24;-10;45;Weapon_AK47;Rifle Soldier/M16A2 Assault Rifle;Rifle Soldier/AK-47 Assault Rifle
	if ( $line =~ m/KILLED;SOLDIER;\d+;([^;]+);-?\d+;-?\d+;-?\d+;-?\d+;(\d+);[^;]+;-?\d+;-?\d+;-?\d+;-?\d+;/i )
	{
		my @splitdata = split(/\;/, $line);

		if ($splitdata[3] eq $DanielPreset)
		{
			POE::Session->create
			( inline_states =>
			{
				_start => sub
				{
					# Waiting for players to load map..
					$_[HEAP]->{next_alarm_time} = int( time() ) + 2;
					$_[KERNEL]->alarm( tick => $_[HEAP]->{next_alarm_time} );
				},
				tick => sub
				{
					plugin::ircmsg("7 $player died as $DanielPreset.", "A");
					$isDanielAlive = 0;

					my $player = plugin::getPlayerFromObjectID($splitdata[2]);
					my $playerID = playerData::playerNameToID($player);

					my ( $result, %playerData ) = playerData::getPlayerData( $player );

					#foreach $key (keys %playerData)
					#{
					#  $value = $prices{$key};
					#  plugin::ircmsg("7DEBUG: $key $value", "A");
					#}

					my $storedPoints = $playerData{'score'};

					plugin::RenRemCMD("team $playerID 0");
					plugin::RenRemCMD("givepoints $playerID $storedPoints");
				}
			}
			);


		}
		elsif ($splitdata[3] eq $YuriPreset)
		{
			POE::Session->create
			( inline_states =>
			{
				_start => sub
				{
					# Waiting for players to load map..
					$_[HEAP]->{next_alarm_time} = int( time() ) + 2;
					$_[KERNEL]->alarm( tick => $_[HEAP]->{next_alarm_time} );
				},
				tick => sub
				{
					plugin::ircmsg("7 $player died as $YuriPreset.", "A");
					$isYuriAlive = 0;

					my $player = plugin::getPlayerFromObjectID($splitdata[2]);
					my $playerID = playerData::playerNameToID($player);

					my ( $result, %playerData ) = playerData::getPlayerData( $player );

					#foreach $key (keys %playerData)
					#{
					#  $value = $prices{$key};
					#  plugin::ircmsg("7DEBUG: $key $value", "A");
					#}

					my $storedPoints = $playerData{'score'};
					plugin::RenRemCMD("team $playerID 1");
					plugin::RenRemCMD("givepoints $playerID $storedPoints");
				}
			}
			);
		}		
	}
}

sub databaseCheck
{
	# Table definition
	my %tables = (
		plugin_molotov => "CREATE TABLE plugin_molotov(ID INTEGER PRIMARY KEY AUTOINCREMENT, danielPlayer TEXT, yuriPlayer TEXT, timestamp INTEGER)"
	);

	# Check which tables already exist
	my @db_tables = plugin::execute_query("SELECT name FROM sqlite_master");
  	foreach (@db_tables)
    {
    	delete $tables{$_->{'name'}}; 
 	}

 	# Create any missing tables
	while ( my($table,$definition) = each %tables )
	{
		plugin::console_output("[Molotov] Creating missing table " . $table);
		plugin::execute_query($definition, 1);
		delete $tables{$table};
	}

	return 1;
}

# Plugin loaded OK
1;