#
# Auto Auth plugin for BRenBot 1.53 by Daniel Paul
#
# Automatically authenticates users if they have joined from the same IP as they used to authenticate before
# the plugin XML configuration file
#
# Version 1.00
#
package autoauth;

use POE;
use plugin;
use Net::DNS::Resolver;
use modules;
use DBI;
use POSIX;

# define additional events in the POE session
our %additional_events = (
 "tryauth" => "tryauth",
 "new_auth" => "new_auth"
);
my $version = main::BR_VERSION;
if ( $version < 1.51 )
{
	print "Warnings plugin $currentVersion cannot run on a BRenBot version lower than 1.51. Please use version 1.42 with BRenBot 1.50\n";
	return 0;
}

# BRenBot automatically sets the plugin name
our $plugin_name;

# BRenBot automatically imports the plugin's config (from the xml file) into %config

my $currentVersion = '1.03';
my $loaded_ok = undef;
my $dnsresolver;
my $sth;						# Database connection
our $dbVersion;

########### Event handlers


sub start
{
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	my %args = %{$args};
	
	# Set our current version in the globals table
	plugin::set_global ( "version_plugin_AutoAuth", $currentVersion );
	
	# Now connect to, and if necessary create, the database
	my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$dbTime,$ctime,$blksize,$blocks) = stat('auth.dat');
	
	$sth = DBI->connect("dbi:SQLite:dbname=auth.dat","","");
		
	if( !$dbTime )
	{
		print "AutoAuth PLUGIN :: Database does not exist, creating fresh database\n";
		
		$sth->do(
			"CREATE TABLE ip
			(name				char(128)	PRIMARY KEY
			,LastIP			char(15))"
		);
		
 		
		$sth->do( "CREATE TABLE AutoAuth ( version int PRIMARY KEY)" );
 		
		$sth->do( "INSERT INTO AutoAuth(version) VALUES ($currentVersion)" );
	}
	return ($loaded_ok = 1);
}


sub stop
{
  undef $dnsresolver;
  	# Comamnds to run when BRenBot quits
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	
	# Comamnds to run when BRenBot quits
	$sth->disconnect
}

sub command
{
  return if ($loaded_ok == 0);

  my %args = %{$_[ARG0]};
  $_[KERNEL]->yield($args{'command'} => \%args);
}


sub playerjoin
{
  my %args = %{$_[ ARG0 ]};
     $poe_kernel->alarm( tryauth => (int(time()) +3) => $args{'nick'} ); 
}

# Triggered by the timer on playerjoin event
sub tryauth
{
  my $playername = $_[ ARG0 ];

    my ( $result_p, %player ) = plugin::getPlayerData( $playername );
	if ( $result_p != 1 || !$player{'ip'} )
		{
			return;
		}

    my $ip;
	my $query;
	# Check them against the IP list
	$query = $sth->prepare (" SELECT LastIP FROM ip WHERE name = \"$playername\""  );
	$query->execute();
#	my @result = $sth->fetchrow_array();
	my @result;
	#my @result = $sth->fetchrow_array;
	while (@result = $query->fetchrow_array)
	{
		if(length($result[0]) >= 7 && $result[0])
		{
			$ip=$result[0];
		}
	}
	if(!$ip)
	{
		return;
	}
   
    # Auth
    if ( $ip eq $player{'ip'} )
    {
      plugin::call_command("BRenBot", "!normal_auth ".$playername);
      plugin::ircmsg ( "Automatically authenticated $playername based on their IP", "A" );
    }
  
  return;
}

sub new_auth
{
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	my %args = %{$args};
	#commmand can only be used from IRC
	return if ($args{'nicktype'} == 0);
	 # Check they supplied a name to search for
	if ( !$args{'arg1'} )
	{
		plugin::ircmsg ( "No player defined. The usage of this command is !new_auth <playername>", $args{ircChannelCode} );
		show_syntax_error(@_);

    return;
	}
	# Check if the player is in the game
	my ($result, %player) = plugin::getPlayerData($args{'arg1'});
	if ($result == 1)
	{
		my $playername = $player{'name'};
		plugin::call_command("BRenBot", "!normal_auth ".$playername);
		plugin::ircmsg ( "Player $playername has been authenticated", $args{ircChannelCode} );
		if ($player{'ip'} )
		{
			my $ip = $player{'ip'};
			my $query;
			# update stored IP
			$query = $sth->prepare (" SELECT LastIP FROM ip WHERE name = \"$playername\""  );
			$query->execute();
			my @result;
			my $ip_db;
			#my @result = $sth->fetchrow_array;
			while (@result = $query->fetchrow_array)
			{
				if(length($result[0]) >= 7 && $result[0])
				{
					$ip_db=$result[0];
				}
			}

			my $query2;
			if($ip_db != 0 and $ip_db ne $ip)
			{
					$query2 = $sth->prepare ( "UPDATE ip SET LastIP = \"$ip\" WHERE name = \"$playername\"" );
			}
			elsif(length($ip_db) == 0) {
				$query2 = $sth->prepare ( "INSERT INTO ip values(\"$playername\",\"$ip\")" );
				}
			if(length($query2) > 0)
			{
				$query2->execute();
			}
		}
	}
	else
	{
		plugin::ircmsg ( "Player $args{'arg1'} was not found in-game", $args{ircChannelCode} );
	}
	return;
}

# Return true or the bot will not work properly...
1;