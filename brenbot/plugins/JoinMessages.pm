package JoinMessages;

use POE;
use plugin;

my $currentVersion = '1.0';
my $loaded_ok = undef;

if ( !defined(plugin::getBrVersion) || plugin::getBrVersion() < 1.53 || (plugin::getBrVersion() == 1.53 && plugin::getBrBuild() < 14 ) )
{
  print " == Join Messages Plugin Error ==\n This version of the plugin is not compatible with BRenBot versions older than 1.53.14\n";
  return 0;
}

our $plugin_name;
our %config;

our %additional_events =
(
  'setjoin'     => 'setjoin',
  'setjoincolor' => 'setjoincolor',
  'viewjoin'    => 'viewjoin'
);

sub start
{
  return ($loaded_ok = 1);
}

sub stop{ }

sub command
{
  return if ($loaded_ok == 0);

  my %args = %{$_[ARG0]};
  $_[KERNEL]->yield ( $args{'command'} => \%args );
}

sub playerjoin
{
  return if ($loaded_ok == 0);

  my %args = %{$_[ARG0]};

  my @joinmessage = get_joinmessage($args{'nick'});

  if ( scalar(@joinmessage) > 0)
    { 
        if ($joinmessage[0]->{'joinmessage'} ne "")
        {
            plugin::RenRemCMD("cmsg $joinmessage[0]->{'redcolor'},$joinmessage[0]->{'greencolor'},$joinmessage[0]->{'bluecolor'} [$args{'nick'}] $joinmessage[0]->{'joinmessage'}");
        }
    }
}

# --------------------------------------------------------------------------------------------------
##
#### Commands
##
# --------------------------------------------------------------------------------------------------

sub setjoin
{
  my %args = %{$_[ ARG0 ]};

  return if ( $args{'nicktype'} == 1 );

  if ($args{arg} =~ /^\!\w+\s*(\S(.+))/i)
  {
    my $setjoin_msg = substr( $1, 0, 249 ); # Limit length to prevent FDS crash
    set_joinmessage( $args{'nick'}, $setjoin_msg );
  }
}

sub setjoincolor
{
    my %args = %{$_[ ARG0 ]};

    return if ( $args{'nicktype'} == 1 );

    set_joinmessagecolors($args{'nick'}, $args{'arg1'}, $args{'arg2'}, $args{'arg3'});

}

# !viewjoin command - Shows a user their current join message
sub viewjoin
{
  my %args = %{$_[ ARG0 ]};
  return if ( $args{'nicktype'} == 1 );

  my @joinmessage = get_joinmessage($args{'nick'});

  if ( scalar(@joinmessage) > 0)
    { 
        if ($joinmessage[0]->{'joinmessage'} ne "")
        {
            plugin::RenRemCMD("cmsg $joinmessage[0]->{'redcolor'},$joinmessage[0]->{'greencolor'},$joinmessage[0]->{'bluecolor'} [$args{'nick'}] $joinmessage[0]->{'joinmessage'}");
        }
    }
}

# --------------------------------------------------------------------------------------------------
##
#### Database Functions
##
# --------------------------------------------------------------------------------------------------

sub set_joinmessage
{
  my $name = lc(shift);               $name =~ s/'/''/g;
  my $message = shift;                $message =~ s/'/''/g;

  if ( !defined(get_joinmessage($name)) )
    { plugin::execute_query( "INSERT INTO playerinfo ( name, joinmessage, redcolor, greencolor, bluecolor) VALUES ( '$name','$message', 255, 255, 255)", 1 ); }
  else
    { plugin::execute_query( "UPDATE playerinfo SET joinmessage = '$message' WHERE LOWER(name) = '$name'", 1 ); }
}

sub set_joinmessagecolors
{
    my $name = lc(shift);
    my $redcolor = shift;
    my $greencolor = shift;
    my $bluecolor = shift;
    

    plugin::execute_query( "UPDATE playerinfo SET redcolor = '$redcolor', bluecolor = '$bluecolor', greencolor = '$greencolor' WHERE LOWER(name) = '$name'", 1 );
}

sub get_joinmessage
{
  my $name = lc(shift);               $name =~ s/'/''/g;

  my @array = plugin::execute_query( "SELECT * FROM playerinfo WHERE LOWER(name) = '$name'" );
  return @array if ( scalar(@array) > 0 );
  return undef;
}

1;