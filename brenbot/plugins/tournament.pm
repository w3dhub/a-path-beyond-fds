﻿#
# TOURNAMENT PLUGIN for BRenBot 1.55
#
# W3D HUB - Made by Nonamen and Silverlight
# \version 1.01
#

package tournament;

use POE;
use plugin;
use List::Util qw/shuffle/;

my $currentVersion = '1.00';
my $loaded_ok = undef;

# Check bot compatibility
if ( !defined(plugin::getBrVersion) || plugin::getBrVersion() < 1.53
  || (plugin::getBrVersion() == 1.53 && plugin::getBrBuild() < 18 ) )
{
  print ' == TOURNAMENT Plugin Error ==\n'
      . 'This version of the plugin is not compatible with BRenBot versions older than 1.53.18\n';
  return 0;
}

# --------------------------------------------------------------------------------------------------

# BRenBot supplies these on plugin startup
our $plugin_name;
our %config;

# Define additional events in the POE session
our %additional_events =
(
  'tournament'        => 'tournament',
  'lastmanstanding'  => 'lastmanstanding',

  # Gamelog regex hooks
  "GL_InfantryCreated" => "GL_InfantryCreated"

);

# Global variables
my $tournament = 0;
my $LastManStanding = 0;
my $preset = "";



# --------------------------------------------------------------------------------------------------
##
#### Bot Events
##
# --------------------------------------------------------------------------------------------------

sub start
{
  plugin::set_global("version_plugin_swap", $currentVersion);

  return ($loaded_ok = 1);
}

sub stop
{
}




# --------------------------------------------------------------------------------------------------
##
#### Game Events
##
# --------------------------------------------------------------------------------------------------

sub command
{
  return if ($loaded_ok == 0);

  my %args = %{$_[ARG0]};
  $_[KERNEL]->yield($args{'command'} => \%args);
}

sub playerjoin
{
  return if ($loaded_ok == 0);
  
  my %args = %{@_[ ARG0 ]};
  
  # Verificar que el modo torneo está activado
  if ($tournament == 1 || $LastManStanding == 1)  {
  
	# Obtener el ID	
	my $id = playerData::playerNameToID($args{'nick'});
  
	plugin::RenRemCMD("ChangeChar $id $preset");
  }
}

sub gameresult
{
	return if ($loaded_ok == 0);
	$tournament = 0;
	$LastManStanding = 0;
}

sub mapload
{
	return if ($loaded_ok == 0);

	POE::Session->create
	( inline_states =>
	{
		_start => sub
		{
			# Waiting for players to load map..
			$_[HEAP]->{next_alarm_time} = int( time() ) + 10;
			$_[KERNEL]->alarm( tick => $_[HEAP]->{next_alarm_time} );
		},
		tick => sub
		{
			my ( undef, $currentMap, undef, undef, undef, undef, undef, undef, undef, undef ) = plugin::getGameStatus();
			if ( $currentMap eq "$config{'MapName1'}") { $tournament = 1; }

			if ($tournament)
			{
				my @presets = ("$config{'AllInfantry1'}", "$config{'AllInfantry2'}", "$config{'AllInfantry3'}", "$config{'AllInfantry4'}", "$config{'AllInfantry5'}", "$config{'SovInfantry1'}", "$config{'SovInfantry2'}", "$config{'SovInfantry3'}", "$config{'SovInfantry4'}", "$config{'SovInfantry5'}", "$config{'SovInfantry6'}", "$config{'SovInfantry7'}");
				
				my @randompresets = shuffle(@presets);
				$preset = "$randompresets[0]";
				my $translatedPreset = plugin::translatePreset ( $preset );

				plugin::serverMsg("Starting $translatedPreset Tournament on map $currentMap!");
				my %playerlist = playerData::getPlayerList();
				while ( my ( $id, $player ) = each ( %playerlist ) )
				{
					plugin::RenRemCMD("eject $player->{'id'}");
					plugin::RenRemCMD("ChangeChar $player->{'id'} $preset");
		
				}
				undef @presets;
				undef @randompresets;
			}
		}
	} );
}


# --------------------------------------------------------------------------------------------------
##
#### Commands
##
# --------------------------------------------------------------------------------------------------

sub tournament
{
  my %args = %{@_[ ARG0 ]};

  if ( !$args{'arg1'} )
  {
	$tournament = 0;
	plugin::serverMsg("Tournament mode has been deactivated!");
  }

  else
  {
	$tournament = 1;
	$preset = "$args{'arg1'}";
	plugin::serverMsg("Tournament mode has been activated!");

	my %playerlist = playerData::getPlayerList();
	while ( my ( $id, $player ) = each ( %playerlist ) )
	{
		plugin::RenRemCMD("eject $player->{'id'}");
		plugin::RenRemCMD("ChangeChar $player->{'id'} $args{'arg1'}");
		
	} # end of foreach playerlist
  }

}

sub lastmanstanding
{
	my %args = %{@_[ ARG0 ]};

	if ( !$args{'arg1'} )
	{
		$LastManStanding = 0;
		plugin::serverMsg("Last Man Standing mode has been deactivated!");
	}
	else
	{
		$LastManStanding = 1;
		$preset = "$args{'arg1'}";
		plugin::serverMsg("Last Man Standing mode has been activated!");
		
		my %playerlist = playerData::getPlayerList();
		while ( my ( $id, $player ) = each ( %playerlist ) )
		{
			plugin::RenRemCMD("eject $player->{'id'}");
			plugin::RenRemCMD("ChangeChar $player->{'id'} $args{'arg1'}");
		
		}	
	}
}


# --------------------------------------------------------------------------------------------------
##
#### Utility Functions
##
# --------------------------------------------------------------------------------------------------

# Infantry kill detected
sub GL_InfantryCreated
{
	if ($tournament != 1 && $LastManStanding != 1) {return;}
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	my $line = $args->{line};

	# [20:20:07] CREATED;SOLDIER;1500004664;Soviet_Engineer;-285;73;-26;-137;50;50;0;Jugador1
	if ( $line =~ m/CREATED;SOLDIER;/i )
	{
		my @splitdata = split(/\;/, $line);
		if ($splitdata[3] eq $preset) {return;}
		

		if($splitdata[3] eq "CyCom" || $splitdata[3] eq "Allied_Ghost" || $splitdata[3] eq "Soviet_Ghost") {return;}		

		my $player = "$splitdata[11]";
		if ( defined ( $player ) )
		{
			my $id = playerData::playerNameToID($player);
			my $AlliedGhost = "Allied_Ghost";
			my $SovietGhost = "Soviet_Ghost";

			if ($tournament == 1)
			{
				plugin::RenRemCMD("eject $id");
				plugin::RenRemCMD("ChangeChar $id $preset");
			}

			if ($LastManStanding == 1)
			{
				my ($SilverResult, %Player) = plugin::getPlayerData($player);
				
				plugin::RenRemCMD("eject $id");
				
				if ($Player{id} == 1) {				
					plugin::RenRemCMD("ChangeChar $id $SovietGhost");
				} else {
					plugin::RenRemCMD("ChangeChar $id $AlliedGhost");
				}
								
			}
		}
	}
	undef $line;
}


# Plugin loaded OK
1;