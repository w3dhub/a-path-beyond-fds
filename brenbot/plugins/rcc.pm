#
# REALCONSOLECOMMANDS
# PLUGIN for EVA
# 

package rcc;

use POE;
use plugin;
use playerData;
use strict;
use DBI;


# Additional Events
our %additional_events =
(	
	# Events for commands - Index name matches full command name

	"timem" => "timem",
	"pip" => "pip",
	"botcount" => "botcount"
);

# Database handle
my $dbh;

my $loaded_ok = undef;

# Check bot compatibility
if ( !defined(plugin::getBrVersion) || plugin::getBrVersion() < 1.53
  || (plugin::getBrVersion() == 1.53 && plugin::getBrBuild() < 15 ) )
{
  print ' == Donate Plugin Error ==\n'
      . 'This version of the plugin is not compatible with BRenBot versions older than 1.53.15\n';
  return 0;
}

# Name and config to be filled in by EVA
our $plugin_name;
our %config;




################################
# Event Functions
#
# Functions for events
################################

# Start event - Triggered on bot startup
sub start
{
	my ( $kernel, $session, $heap ) = @_[ KERNEL, SESSION, HEAP ];
	
	# Run startup functions
	loadDatabase();
  return ($loaded_ok = 1);
}

# Stop event - Triggered on bot shutdown
sub stop
{
	my ( $kernel, $session, $heap ) = @_[ KERNEL, SESSION, HEAP ];
	
	$dbh->disconnect;
}

# Command event - Triggers on any !command which is in this plugins XML file
sub command
{
	my ( $kernel, $args ) = @_[ KERNEL, ARG0 ];
	my %args = %{$args};
	
	# Trigger the event with the same name as the command
	if ( $args{'command'} eq "update" )
		{ $kernel->yield ( update => 0 ); }
	else
		{ $kernel->yield ( $args{'command'} => \%args ); }
}

sub playerjoin
{
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	my %args = %{$args};

	my $sth = $dbh->prepare("SELECT * FROM  MutedPlayers WHERE name = '" . $args{'nick'} . "'");
	$sth->execute();
	my $registro = $sth->fetchrow_hashref();
	if ($registro > 0) {
		my $nombre =  $registro->{name};
		my $id = playerData::playerNameToID($nombre);
		plugin::RenRemCMD ( "mute $id", );
		plugin::RenRemCMD ( "cmsg 255,127,0 [EVA] $nombre is still muted.", );
		plugin::ircmsg ("14Host: 7[EVA] $nombre is still muted.", $args{'ircChannelCode'});
	}
}


################################
# Command Functions
#
# Functions for each !command
################################

sub timem

{
	my ( $session, $heap, $args ) = @_[ SESSION, HEAP, ARG0 ];
	my $kernel = $_[KERNEL];
	my %args = %{$args};
	my $Timem;
	if($args{'arg1'})
	{
		$Timem = ($args{'arg1'} * 60);
		plugin::RenRemCMD( "time $Timem" );
		plugin::RenRemCMD("msg server time changed to $args{'arg1'} minutes")
	}
	else
	{
		if ( $args{'nicktype'} == 2 )
		{
			plugin::ircmsg( 'Please use a number to set the time limit.', $args{'ircChannelCode'} ); 
		}
		else 
		{
			plugin::pagePlayer( $args{'nick'}, 'BRenBot', 'Please use a number to set the time limit' );
			plugin::ircmsg( 'Please use a number to set the time limit.', $args{'ircChannelCode'} ); 
		}
	}

}

sub botcount

{
	my ( $session, $heap, $args ) = @_[ SESSION, HEAP, ARG0 ];
	my $kernel = $_[KERNEL];
	my %args = %{$args};
	if($args{'arg1'})
	{
		plugin::RenRemCMD( "botcount $args{'arg1'}" );
		plugin::RenRemCMD("msg server botcount changed to $args{'arg1'}")
	}
	else
	{
		if ( $args{'nicktype'} == 2 )
		{
			plugin::ircmsg( 'Please use a number to set the aount of bots.', $args{'ircChannelCode'} ); 
		}
		else 
		{
			plugin::pagePlayer( $args{'nick'}, 'BRenBot', 'Please use a number to set the bot count' );
			plugin::ircmsg( 'Please use a number to set the bot couunt.', $args{'ircChannelCode'} ); 
		}
	}

}






sub pip
{
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	my %args = %{$args};
	
	if ( !$args{'arg1'} )
	{
		if ( $args{'nicktype'} == 1 ) { plugin::ircmsg ( "7Usage: !ip <playername>", $args{'ircChannelCode'} ); return; }
		else
		{ 
			my $nombre = $args{'nick'};
			my ( $result, %player ) = playerData::getPlayerData ( $nombre );
			plugin::pagePlayer ( $args{'nick'}, "brenbot", "Usage: !ip <playername>" );
		}
	}
	else
	{
		my $nombre = $args{'arg1'};
		my ( $result, %player ) = playerData::getPlayerData ( $nombre );
		if ($result == 0)
		{ 
			if ( $args{'nicktype'} == 1 )
			{
				plugin::ircmsg ("7$args{'arg1'} was not found in-game, or it" ."'s not unique.", "A" );
			}
			else
			{
				plugin::pagePlayer ( $args{'nick'}, "brenbot", "$args{'arg1'} was not found in-game, or it" ."'s not unique." );
				#plugin::playsndp( "interface_alert1.wav", $args{'nick'} );
			}
		}
		else
		{
			$args{'arg'} =~ m/^\!\S+\s(.+)$/i;
		
			if ( $args{'nicktype'} == 1 )
			{
				
				plugin::ircmsg ("7 $player{'name'} his ip is $player{'ip'}", "A" );
			}
			else
			{
				plugin::pagePlayer ( $args{'nick'}, "brenbot", "you can only use this commands from irc." );

			}
		}
	}
}


################################
# Other Functions
#
# Startup functions
################################

# Loads and checks the database
sub loadDatabase
{
	my $newDB = 1;
	if (-e "mute.dat" ) { $newDB = 0; }
	
	if ( $^O eq "MSWin32" )
	{
		$dbh = DBI->connect("dbi:SQLite:dbname=mute.dat","","");
	}
	else	# Use alternative SQLite driver for linux
	{
		$dbh = DBI->connect("dbi:SQLite:dbname=mute.dat","","");
	}

	# If $newDB == 1 then we are creating a new database
	if( $newDB == 1 )
	{
		$dbh->do(
			"CREATE TABLE MutedPlayers
			 (name				char(128) PRIMARY KEY
			  )" );
			 
	}
	
}


1;