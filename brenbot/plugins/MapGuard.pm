#
# MapGuard plugin for W3D Hub
# Allows random map rotations
#
# \author Silverlight (cdpm42@gmail.com)
# \version 1.00
#

package MapGuard;

use POE;
use plugin;
use modules;
use brconfig;
use brdatabase;

my $loaded_ok = undef;
my $cachedCurrentMap;

# Check bot compatibility
if ( !defined(plugin::getBrVersion) || plugin::getBrVersion() < 1.53 || (plugin::getBrVersion() == 1.53 && plugin::getBrBuild() < 18 ) )
{
	print ' == MapGuard Plugin Error ==\n' . 'This version of the plugin is not compatible with BRenBot versions older than 1.53.18\n';
	return 0;
}

# BRenBot supplies these on plugin startup
our $plugin_name;
our %config;

# Define additional events in the POE session
our %additional_events =
(
	'newSetNextMap'				 => 'newSetNextMap',
	'displayBPCQ' => 'displayBPCQ',
	'displayLPCQ' => 'displayLPCQ',
	'displayHPCQ' => 'displayHPCQ'
);

my @BotsPlayerCountMaps;
my @LowPlayerCountMaps;
my @HighPlayerCountMaps;
my @allMaps;
my @BotsPlayerCountMapsQueue;
my @LowPlayerCountMapsQueue;
my @HighPlayerCountMapsQueue;

# --------------------------------------------------------------------------------------------------
##
#### Bot Events
##
# --------------------------------------------------------------------------------------------------

sub start
{
	@BotsPlayerCountMaps = Get_Array_From_Config("botsPlayerCountMaps");
	@LowPlayerCountMaps = Get_Array_From_Config("lowPlayerCountMaps");
	@HighPlayerCountMaps = Get_Array_From_Config("highPlayerCountMaps");
	@allMaps = (@BotsPlayerCountMaps, @LowPlayerCountMaps, @HighPlayerCountMaps);
	@BotsPlayerCountMapsQueue = @BotsPlayerCountMaps;
	@LowPlayerCountMapsQueue = @LowPlayerCountMaps;
	@HighPlayerCountMapsQueue = @HighPlayerCountMaps;
	return ($loaded_ok = 1);
}

sub stop {}

# --------------------------------------------------------------------------------------------------
##
#### Game Events
##
# --------------------------------------------------------------------------------------------------

sub command
{
	return if ($loaded_ok == 0);

	my %args = %{$_[ARG0]};
	$_[KERNEL]->yield ( $args{'command'} => \%args );
}

sub mapload
{
	return if ($loaded_ok == 0);

	POE::Session->create
	( inline_states =>
		{
			_start => sub
			{
				# Waiting for players to load map..
				$_[HEAP]->{next_alarm_time} = int( time() ) + 1;
				$_[KERNEL]->alarm( tick => $_[HEAP]->{next_alarm_time} );
			},
			tick => sub
			{
				my ( undef, $currentMap, undef, undef, undef, undef, $currentPlayers, undef, $timeRemaining, undef ) = serverStatus::getGameStatus();
				$cachedCurrentMap = substr($currentMap, 0, -4);

				if ($config{'botsEnabled'} && $currentPlayers < $config{'lowPlayerCountNumber'})
				{
					@BotsPlayerCountMapsQueue = autoSetNextMap("BotsPlayerCountMode", \@BotsPlayerCountMapsQueue, \@BotsPlayerCountMaps);
					if ($cachedCurrentMap ~~ @BotsPlayerCountMaps)
					{
						my $count = $config{'botCount'};
						plugin::RenRemCMD ( "botcount $count", );
					}
				}
				elsif ($currentPlayers < $config{'highPlayerCountNumber'})
				{
					@LowPlayerCountMapsQueue = autoSetNextMap("LowPlayerCountMode", \@LowPlayerCountMapsQueue, \@LowPlayerCountMaps);
					plugin::RenRemCMD ( "botcount 0", );
				}
				else
				{
					@HighPlayerCountMapsQueue = autoSetNextMap("HighPlayerCountMode", \@HighPlayerCountMapsQueue, \@HighPlayerCountMaps);
					plugin::RenRemCMD ( "botcount 0", );
				}
			}
		} 
	);
}

sub autoSetNextMap
{
	my ($modeName, $mapQueue_ref, $mapQueueFull_ref) = @_;
	my @mapQueue = @{ $mapQueue_ref }; # dereferencing and copying each array
	my @mapQueueFull = @{ $mapQueueFull_ref };
	brIRC::ircmsg ( "$modeName Enabled", "A" );
	
	# Make sure queue is not empty
	if (scalar(@mapQueue) == 0)
	{
		brIRC::ircmsg ( "Queue is empty, resetting to full list.", "A" );
		@mapQueue = @mapQueueFull;
		# Remove current map from rotation so it cannot be played 2x in a row (as last of previous list and first of new list)
		@mapQueue = removeMapFromLists($cachedCurrentMap, \@mapQueue);
	}

	my $arraySize		= @mapQueue;
	my $randomNumber	= int(rand($arraySize));
	my $mapname			= "$mapQueue[$randomNumber]";
	
	brIRC::ircmsg ( "Random seed: $randomNumber", "A" );
	brIRC::ircmsg ( "Queue size: $arraySize", "A" );


	# Get the nextmap
	my $nextmap = modules::GetNextMap();

	# Send a RenRemCMD to instruct the Resource Manager to change the next map
	modules::console_output ( "Setting map at index $nextmap->{id} to $mapname." );
	RenRem::RenRemCMD ( "mlistc $nextmap->{id} $mapname" );

	if ( !brdatabase::get_global( 'nextmap_changed' ) )
	{
		print "DEBUG: Recording map change details\n";
		brdatabase::set_global( 'nextmap_changed', '4' );

		# Record the details of the map we replaced
		brdatabase::set_global( 'nextmap_changed_id', $nextmap->{'id'} );
		brdatabase::set_global( 'nextmap_changed_mapname', $mapname );
	}

	# Update the rotation
	foreach (@brconfig::maplist)
	{
		if ( $_->{'id'} == $nextmap->{'id'} )
		{
			$_->{'mapname'} = $mapname;
			last;
		}
	}				

	@mapQueue = removeMapFromLists($mapname, \@mapQueue);
	return @mapQueue;
}

sub removeMapFromLists {
	my ($mapname, $mapQueue_ref) = @_; # reading parameters
	my @mapQueue = @{ $mapQueue_ref }; # dereferencing and copying each array
	# Delete the map from all queues and current too, it gets returned
	if ($mapname ~~ @mapQueue)
	{
		my $index = 0;
		$index++ until $mapQueue[$index] eq $mapname;
		splice(@mapQueue, $index, 1);
	}
	
	if ($mapname ~~ @BotsPlayerCountMapsQueue)
	{
		my $index = 0;
		$index++ until $BotsPlayerCountMapsQueue[$index] eq $mapname;
		splice(@BotsPlayerCountMapsQueue, $index, 1);
	}
	
	if ($mapname ~~ @LowPlayerCountMapsQueue)
	{
		my $index = 0;
		$index++ until $LowPlayerCountMapsQueue[$index] eq $mapname;
		splice(@LowPlayerCountMapsQueue, $index, 1);
	}
	
	if ($mapname ~~ @HighPlayerCountMapsQueue)
	{
		my $index = 0;
		$index++ until $HighPlayerCountMapsQueue[$index] eq $mapname;
		splice(@HighPlayerCountMapsQueue, $index, 1);
	}

	return @mapQueue;
}

sub displayBPCQ { displayMapsLeft(@BotsPlayerCountMapsQueue); }

sub displayLPCQ { displayMapsLeft(@LowPlayerCountMapsQueue); }

sub displayHPCQ { displayMapsLeft(@HighPlayerCountMapsQueue); }

sub displayMapsLeft
{
	my (@mapQueue) = @_; # reading parameters
	for (my $i =0; $i < scalar(@mapQueue); $i +=4 )
	{
		my $mapName1 = ($i < scalar(@mapQueue)) ? $mapQueue[$i] : ""; 
			
		my $mapName2 = ($i+1 < scalar(@mapQueue)) ? $mapQueue[$i+1] : "";

		my $mapName3 = ($i+2 < scalar(@mapQueue)) ? $mapQueue[$i+2] : "";

		my $mapName4 = ($i+3 < scalar(@mapQueue)) ? $mapQueue[$i+3] : "";
			
		plugin::ircmsg ("$mapName1 $mapName2 $mapName3 $mapName4", "A" );
	}
}

sub newSetNextMap{

	my %args		= %{@_[ ARG0 ]};
	my $mapname		= $args{'arg1'};
	my $foundMap	= 0;

	if (length($mapname) < 4)
	{
		plugin::ircmsg ("7NSNM: Mapname must be at least 4 characters long!", "A" );
		plugin::serverMsg("Mapname must be at least 4 characters long!");
		return;
	}

	foreach (@allMaps)
	{
		if (index($_, $mapname) != -1)
		{
			$mapname = $_;
			$foundMap = 1;
		}
	}

	if ($foundMap == 0)
	{
		plugin::ircmsg ("7Map not found!", "A" );	
		plugin::serverMsg("Map not found!");
		return;
	}

	# Get the nextmap
	my $nextmap = modules::GetNextMap();

	# Send a RenRemCMD to instruct the Resource Manager to change the next map
	modules::console_output ( "Setting map at index $nextmap->{id} to $mapname." );
	RenRem::RenRemCMD ( "mlistc $nextmap->{id} $mapname" );
	
	if ( !brdatabase::get_global( 'nextmap_changed' ) )
	{
		print "DEBUG: Recording map change details\n";
		brdatabase::set_global( 'nextmap_changed', '4' );

		# Record the details of the map we replaced
		brdatabase::set_global( 'nextmap_changed_id', $nextmap->{'id'} );
		brdatabase::set_global( 'nextmap_changed_mapname', $nextmap->{'mapname'} );
	}

	plugin::serverMsg("The next map will be: $mapname");

	# Update the rotation
	foreach (@brconfig::maplist)
	{
		if ( $_->{'id'} == $nextmap->{'id'} )
		{
			$_->{'mapname'} = $mapname;
			last;
		}
	}
}

sub Get_Array_From_Config
{
	my ($paramName) = @_; # reading parameters
	Ensure_Config_Is_Array($paramName);
	
	my @array;
	my $num = 0;
	foreach (@{$config{$paramName}})
	{
		$array[$num] = $_;
		$num++;
	}
	
	return @array;
}

# We want to ensure $config always contains an array, regardless of how many entries there are
sub Ensure_Config_Is_Array
{
	my ($configArrayName) = @_; # reading parameters
	
	my $rawData = $config{$configArrayName};
	$config{$configArrayName} = ();
	
	if (defined($rawData))
	{
		$rawData = [$rawData] if (ref $rawData ne 'ARRAY');
		foreach (@{$rawData})
		{
			push(@{$config{$configArrayName}}, $_) if defined $_;
		}
	}
}

# Plugin loaded OK
1;