#
# Testing plugin for BRenBot 1.53 by moonsense715
#
# Can auto-disable bots on mapstart by commands
#
# Version 1.00
#
package coop;

use POE;
use plugin;

# define additional events in the POE session
our %additional_events = (
	"coop" => "coop",
	"diff_inc" => "diff_inc",
	"diff_dec" => "diff_dec",
);

# BRenBot automatically sets the plugin name
our $plugin_name;

# BRenBot automatically imports the plugin's config (from the xml file) into %config
our %config;

our $currentVersion = 1.00;
my $loaded_ok = undef;
my $coopEnabled = 0;
my $team;
my $enemyTeam;
my $difficulty = 3;


########### Event handlers

sub start
{
	# Set our current version in the globals table
	plugin::set_global ( "version_plugin_testing", $currentVersion );
	
	return ($loaded_ok = 1);
}


sub stop
{

}


# --------------------------------------------------------------------------------------------------
##
#### Game Events
##
# --------------------------------------------------------------------------------------------------

sub command
{
  return if ($loaded_ok == 0);

  my %args = %{$_[ARG0]};
  $_[KERNEL]->yield ( $args{'command'} => \%args );
}


# Mapload event - Triggers when a new map has finished loading
sub mapload
{
	my ( $kernel, $args ) = @_[ KERNEL, ARG0 ];
	my %args = %{$args};

	$coopEnabled = 0;
#	my $rand_team = int(rand(9))%2;
#	print
#	plugin::call_command("BRenBot", "!coop ".$rand_team);
	
}


# Playerjoin event - Triggers when a player joins the server
sub playerjoin
{
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	my %args = %{$args};
	return if !$coopEnabled;

	my ($result, %player) = playerData::getPlayerData($args{'nick'});
	if ($result == 1) {
		plugin::RenRemCMD ( "team2 $player{'id'} $team", );
	}
	
	Update_Botcount();
}


# Playerleave event - Triggers when a player leaves the server
sub playerleave
{
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	my %args = %{$args};
	return if !$coopEnabled;
	Update_Botcount();
}


# --------------------------------------------------------------------------------------------------
##
#### Commands
##
# --------------------------------------------------------------------------------------------------

sub coop
{
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	my %args = %{$args};

	$coopEnabled = 1;
	$args{'arg'} =~ m/^\!\S+\s(.+)$/i;  # reading command parameters
	$team = $1;
	$enemyTeam = $team == 1 ? 0 : 1;
	my $msg = "[Coop Plugin] Activating coop mode for team $team (Difficulty: $difficulty)!";
	plugin::RenRemCMD ( "cmsg 250,200,0 $msg", );
	plugin::ircmsg ( "7$msg", $args{'ircChannelCode'} );
	
	my %playerlist = playerData::getPlayerList();
	while (my ($id, $player) = each (%playerlist)) {
		plugin::RenRemCMD ( "team2 $id $team", );
	}
	
	Update_Botcount();
}

sub diff_inc
{
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	my %args = %{$args};
	return if !$coopEnabled;

	$difficulty++;
	my $msg = "[Coop Plugin] Increasing coop difficulty ($difficulty)!";
	plugin::RenRemCMD ( "cmsg 250,200,0 $msg", );
	plugin::ircmsg ( "7$msg", $args{'ircChannelCode'} );
	Update_Botcount();
}

sub diff_dec
{
	my ( $kernel, $session, $heap, $args ) = @_[ KERNEL, SESSION, HEAP, ARG0 ];
	my %args = %{$args};
	return if !$coopEnabled;

	$difficulty--;
	my $msg = "[Coop Plugin] Decreasing coop difficulty ($difficulty)!";
	plugin::RenRemCMD ( "cmsg 250,200,0 $msg", );
	plugin::ircmsg ( "7$msg", $args{'ircChannelCode'} );
	Update_Botcount();
}

sub Update_Botcount
{
	my ( undef, undef, $gdiPlayers, undef, $nodPlayers, undef, undef, undef, undef, undef ) = plugin::getGameStatus();
	my $totalPlayers = $gdiPlayers + $nodPlayers;
	my $botCount = $totalPlayers * $difficulty + ($totalPlayers >= 3 ? 2 : 0);
	plugin::RenRemCMD ( "botcount $botCount $enemyTeam", );
	print "COOP PLUGIN :: UPDATE BOTCOUNT :: $botCount\n";
}


# Return true or the bot will not work properly...
1;